#!/bin/bash

echo -e "\e[0m\n  ███████╗██╗   ██╗ ██████╗\e[0m\e[34m ███████╗ ██████╗\e[0m ██╗     ██╗   ██╗████████╗██╗ ██████╗ ███╗   ██╗███████╗";
echo -e "  ██╔════╝██║   ██║██╔═══██╗\e[0m\e[34m██╔════╝██╔═══██╗\e[0m██║     ██║   ██║╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝";
echo -e "  █████╗  ██║   ██║██║   ██║\e[0m\e[34m███████╗██║   ██║\e[0m██║     ██║   ██║   ██║   ██║██║   ██║██╔██╗ ██║███████╗";
echo -e "  ██╔══╝  ╚██╗ ██╔╝██║   ██║\e[0m\e[34m╚════██║██║   ██║\e[0m██║     ██║   ██║   ██║   ██║██║   ██║██║╚██╗██║╚════██║";
echo -e "  ███████╗ ╚████╔╝ ╚██████╔╝\e[0m\e[34m███████║╚██████╔╝\e[0m███████╗╚██████╔╝   ██║   ██║╚██████╔╝██║ ╚████║███████║";
echo -e "  ╚══════╝  ╚═══╝   ╚═════╝\e[0m\e[34m ╚══════╝ ╚═════╝\e[0m ╚══════╝ ╚═════╝    ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝";
echo -e "\e[1m\e[33m\n";
echo -e "  888b     d888                                     888               .d8888b.  ";
echo -e "  8888b   d8888                                     888              d88P  Y88b ";
echo -e "  88888b.d88888                                     888                     888 ";
echo -e "  888Y88888P888  8888b.   .d88b.   .d88b.  88888b.  888888 .d88b.         .d88P ";
echo -e "  888 Y888P 888     \"88b d88P\"88b d8P  Y8b 888 \"88b 888   d88\"\"88b    .od888P\"  ";
echo -e "  888  Y8P  888 .d888888 888  888 88888888 888  888 888   888  888   d88P\"      ";
echo -e "  888   \"   888 888  888 Y88b 888 Y8b.     888  888 Y88b. Y88..88P   888\"       ";
echo -e "  888       888 \"Y888888  \"Y88888  \"Y8888  888  888  \"Y888 \"Y88P\"    8888888888  ";
echo -e "                              888                                                  ";
echo -e "                         Y8b d88P                                                  ";
echo -e "                          \"Y88P\"\e[0m";

title() {
    WORD=$@;
    NUMBER_OF_SPACES_SURROUNDING_THE_WORD=1;

    TERMINAL_WIDTH=$(tput cols);
    WORD_SIZE=$(echo $WORD | wc -c );
    NUMBER_OF_SPACES_SURROUNDING_THE_WORD=1;
    NUMBER_OF_SYMBOLS=$((TERMINAL_WIDTH - $WORD_SIZE - 2 * $NUMBER_OF_SPACES_SURROUNDING_THE_WORD));
    LENGTH_OF_RIGHT_PART=$((NUMBER_OF_SYMBOLS/2 + $NUMBER_OF_SYMBOLS % 2));
    LENGTH_OF_LEFT_PART=$(( $TERMINAL_WIDTH - 2 *$NUMBER_OF_SPACES_SURROUNDING_THE_WORD - $WORD_SIZE - $LENGTH_OF_RIGHT_PART));


    echo -e "\n\n\e[46m";
    printf '%*s' "$LENGTH_OF_RIGHT_PART" '' | tr "" ' ';
    printf '%*s' $NUMBER_OF_SPACES_SURROUNDING_THE_WORD;
    echo $WORD;
    printf '%*s' $NUMBER_OF_SPACES_SURROUNDING_THE_WORD;
    printf '%*s' "$LENGTH_OF_RIGHT_PART" '' | tr "" ' ';
    echo -e "\e[0m\n";
}

warning() {
    WORD=$@;
    NUMBER_OF_SPACES_SURROUNDING_THE_WORD=1;

    TERMINAL_WIDTH=$(tput cols);
    WORD_SIZE=$(echo $WORD | wc -c );
    NUMBER_OF_SPACES_SURROUNDING_THE_WORD=1;
    NUMBER_OF_SYMBOLS=$((TERMINAL_WIDTH - $WORD_SIZE - 2 * $NUMBER_OF_SPACES_SURROUNDING_THE_WORD));
    LENGTH_OF_RIGHT_PART=$((NUMBER_OF_SYMBOLS/2 + $NUMBER_OF_SYMBOLS % 2));
    LENGTH_OF_LEFT_PART=$(( $TERMINAL_WIDTH - 2 *$NUMBER_OF_SPACES_SURROUNDING_THE_WORD - $WORD_SIZE - $LENGTH_OF_RIGHT_PART));


    echo -e "\n\n\e[43m";
    printf '%*s' "$LENGTH_OF_RIGHT_PART" '' | tr "" ' ';
    printf '%*s' $NUMBER_OF_SPACES_SURROUNDING_THE_WORD;
    echo $WORD;
    printf '%*s' $NUMBER_OF_SPACES_SURROUNDING_THE_WORD;
    printf '%*s' "$LENGTH_OF_RIGHT_PART" '' | tr "" ' ';
    echo -e "\e[0m\n";
}

warning "LIMPANDO DIRETÓRIOS..." \
&& rm -rfv generated/* pub/static/adminhtml/* pub/static/frontend/* var/importexport/ var/import_history/ var/page_cache/* var/view_preprocessed/* var/cache/* \
&& title "HABILITANDO MODO DE MANUTENÇÃO" \
&& php bin/magento maintenance:enable \
&& warning "APAGANDO TODOS OS CACHES:" \
&& php bin/magento cache:clean \
&& title "INSTALANDO NOVAS VERSÕES DOS MÓDULOS" \
&& php bin/magento setup:upgrade \
&& title "ATUALIZANDO DEPENDÊNCIAS" \
&& php bin/magento setup:di:compile -vvv \
&& title "EXECUTANDO TRADUÇÃO" \
&& php bin/magento i18n:pack --mode=replace --allow-duplicates vendor/magento2translations/language_pt_br/pt_BR.csv pt_BR;

title "GERANDO CONTEÚDOS ESTÁTICOS" \
&& php bin/magento setup:static-content:deploy -f pt_BR en_US \
&& title "INVALIDANDO CACHES" \
&& php bin/magento cache:flush;

title "DESATIVANDO MODO DE MANUTENÇÃO" \
&& php bin/magento maintenance:disable \
&& warning "ALTERANDO PERMISSÕES DO DIRETÓRIO DE CACHE E SESSÃO" \
&& chmod -Rv 775 "./var/cache/" \
&& chmod -Rv 775 "./var/session/";
