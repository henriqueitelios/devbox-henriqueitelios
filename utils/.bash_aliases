# Magento 2
alias henriqueitelios-db="docker exec -it henriqueitelios_database_1 bash";
alias henriqueitelios-mysql="docker exec henriqueitelios_database_1 mysql -umagento -pmagento magento";
alias henriqueitelios-php-exec="docker exec henriqueitelios_php_1";
alias henriqueitelios-php-root="docker exec -it -u0 henriqueitelios_php_1 bash";
alias henriqueitelios-php="docker exec -it henriqueitelios_php_1 bash";
alias henriqueitelios-run="bash ~/Projects/henriqueitelios/utils/run";
alias henriqueitelios-stop="docker-compose -f ~/Projects/henriqueitelios/docker-compose.yml stop";
alias henriqueitelios-up-attached="docker-compose -f ~/Projects/henriqueitelios/docker-compose.yml up";
alias henriqueitelios-up="docker-compose -f ~/Projects/henriqueitelios/docker-compose.yml up -d";

# Utils
alias ll="ls -lhas"
alias docker-ips="docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \$(docker ps -aq)";
alias hg="history | grep"
alias mod="stat -c \"%U.%G %a %n [%F]\""
