Docker Environment Magento 2.2
==============================

Containers for Magento 2 project development environment.

### How To Use

1. Create a directory to keep your projects (just a sugestion)

        $ mkdir ~/Projects/

2. Clone this repository inside said directory

        $ cd ~/Projects/ && git clone git@bitbucket.org:leandro_medeiros/venia-devbox.git venia

3. Copy your SSH keys, which must be configured on your GitHub & BitBucket accounts, into the _utils/ssh/_. If still don't have any keys in your computer please follow [this tutorial](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html) before proceding.

        $ cp ~/.ssh/* ~/Projects/venia/utils/ssh/
        $ chmod 755 ~/Projects/venia/utils/ssh/*

4. If you're installing a Magento Commerce version, edit *auth.json* and insert your Magento keys (optional):

        $ nano ./utils/auth.json

5. Create a virtual host redirecting to 127.0.0.1

        $ sudo nano /etc/hosts

6. \[Optional\] Merge the _./utils/.bash_aliases_ with yours:

        $ cat ~/Projects/venia/utils/.bash_aliases > ~/.bash_aliases
        $ . ~/.bash_aliases

7. Execute the **run** utility to start configuring your development environment, I'm sure you can figure it out after that :thumbs-up:

        $ ~/Projects/venia/utils/run --help

    (if you followed step 6 you can use this instead)

        $ venia-run --help

    ![run-help](readme/readme01.png)

8. ???

9. PROFIT.


### Host Requirements

- Git
- Git Flow
- Git Up (either the utility or the alias)
- Docker
- Docker Compose
- MySQL Client
- Host **venia.test** (127.0.0.1) configured in the _/etc/hosts_ (it can be any other host as long as you edit _./config.env_ file **and** use the `-u` parameter when running the `./utils/run` script).

### Utilities
#
> `$ utils/run --update-vendors`: Updates the custom modules inside the **vendors** directory:
#
![update-vendors](readme/readme02.png)
#
#

> **_./utils/.bash_aliases_** can be copied to your home path to activate the following aliases:

- `mod`: File mode in octal
- `hg`: History | Grep
- `venia-db`: access mysql container
- `venia-mysql`: runs MySQL inside the container
- `venia-php-exec`: runs a command inside the php container
- `venia-php-root`: access php container as root
- `venia-php`: access php container
- `venia-run`: executes the devbox "run" utility
- `venia-stop`: stop all containers
- `venia-up-attached`: start all containers
- `venia-up`: start all containers (detached)
#
#

Activate it with:

        $ cat ~Projects/venia/utils/.bash_aliases > ~/.bash_aliases
#
#

> **_./utils/checkChanges_**: Updates all repositories found in the desired directory (meant to be run in the _./source/vendor/custom_vendor_ dir). Copy to your path for easier use.
#
#


> **PHP Container**

- Command `cleardirs`: Clears some Magento _generated_, _var_ and _pub_ directories.
- Command `compile`: Clears _generated_ directory and compiles Magento.
- Command `deploy`: Clears some Magento _generated_, _var_ and _pub_ directories, flushs the cache, upgrades the modules, compiles Magento, runs translation and runs Grunt Less.
- Command `full-deploy`: Clears some Magento _var_ and _pub_ directories, flushs the cache, upgrades the modules, compiles Magento, runs translation and deploys static content.
- Command `hg`: history | grep
- Command `ll`: ls -lHas
- Command `n98`: Runs n98 utility
- Command `magento`: php bin/magento
- Command `mod`: File mode in octal
- Command `upgrade`: Clears some Magento _var_ directories, flushs the cache and upgrades the modules.
#
#

> **MySQL Container**

- Command `db`: Log's to MySQL selecting the _magento_ database.
